(function () {
    'use strict'
    App.component('aboutContent',{
        controller:['$log', AboutContent],
        controllerAs:'AC',
        templateUrl:'./src/components/about/about.html',
        bindings:{
            heroName:'='
        }
    });

    function AboutContent($log) {
        this.$onInit = function () {
            $log.log('About component start');
            $log.log('HeroName is', this.heroName);
        }
    }
})()