App.component('topHeader', {
    templateUrl:'./src/components/header/header.html',
    controller: ['$scope','$log',TopHeader],
    controllerAs:'TH'
});

function TopHeader($scope,$log) {
    'use_strict'
    this.$onInit = function () {
        $log.log('Init header');
        $log.log($scope);

    }
    var vm = this;
    vm.brand = "My brand";

}
