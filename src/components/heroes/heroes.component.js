App.component('heroesList',{
    controller: ['Heroes','$log',HeroesList],
    controllerAs:'HL',
    templateUrl:'./src/components/heroes/heroes.html'
});

function HeroesList(Heroes, $log) {
    var vm = this;
    vm.heroesList;

    this.$onInit = function () {
        Heroes.getHeroes().then(function (res) {
            vm.heroesList = res;
            $log.log(vm.heroesList);

        })
    }
}