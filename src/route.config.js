App
    .config(['$routeProvider','$routeParams',RouterConfig])
    .config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('');
    }]);

function RouterConfig($routeProvider, $routeParams) {
    $routeProvider
        .when('/about', {
            template: '<about-content></about-content>'
        })
        .when('/',{
            template:`
                <h5>Main</h5>
                <heroes-list></heroes-list>
            `
        })
        .when('/hero/:name',{
            template:`
                <h5>About</h5>
                <heroes-list></heroes-list>
                <about-content hero-name="'asd'"></about-content>
            `
        })
    ;
}