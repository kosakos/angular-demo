(function(angular){
    "use strict";
    angular.module('App')
        .service('Heroes', ['$log', '$http',Heroes]);
    
    function Heroes($log, $http){
        var service = {};

        service.getHeroes = function () {
            return $http.get('heroes.json')
                .then((response) => {
                    return response.data;
                })
                .catch((error) => {
                    this.$log.error('Bad link.\n' + angular.toJson(error.data, true));
                });
        }


        return service;
    }
    
})(angular);